=============================
Managing Artifacts with Heist
=============================

Artifact Upgrades
-----------------

By default, Heist will not upgrade the artifact on a target. If you want to
ensure the artifact is always upgraded set :ref:`dynamic_upgrade`. If
this argument is set, the artifact will be updated in two situations:

  * On startup of Heist, if a target already has an artifact deployed
    it will upgrade if it detects a newer version.
  * While the Heist process is running, it will attempt to check if there
    is a newer version and will upgrade the artifact. This check is done,
    by default every 60 seconds, but can be adjusted with the conf
    :ref:`checkin_time`.
