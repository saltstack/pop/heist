import os
import socket
import tempfile
import time
import unittest.mock as mock
import uuid

import asyncssh
import docker
import pytest


@pytest.fixture(name="network_name", scope="session")
def docker_network_name() -> str:
    if os.environ.get("GITLAB_CI") == "true":
        # For running in docker-in-docker on gitlab-ci
        return "docker"
    else:
        # For running the tests locally
        return "host"


@pytest.fixture(name="host", scope="session")
def docker_host() -> str:
    if os.environ.get("GITLAB_CI") == "true":
        # For running in docker-in-docker on gitlab-ci
        return "docker"
    else:
        # For running the tests locally
        return "localhost"


@pytest.fixture(name="roster", scope="function")
def get_roster(hub) -> dict:
    """
    Return a temporary file that can be used as a roster
    """
    r = {}  # hub.heist.ROSTERS
    yield r


@pytest.fixture(scope="function")
def hub(hub, tmp_path):
    hub.pop.sub.add(dyne_name="heist")

    # Throw exceptions instead of catching them
    hub.heist.HARD_FAIL = True

    heist_conf = tmp_path / "heist.conf"
    with open(heist_conf, "w") as fp:
        pass

    with mock.patch("sys.argv", ["heist", "test", f"--config={heist_conf}"]):
        hub.pop.config.load(
            ["heist", "rend"],
            cli="heist",
            parse_cli=False,
        )

    yield hub


def next_free_port(host, port: int = 2222) -> int:
    # Find the next two available ports to bind to greater than 2222
    for i in range(1000):
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
            sock.settimeout(2)  # Timeout in case the port check hangs
            try:
                sock.bind((host, port))
                break
            except OSError:
                port += 2  # If bind() raises an error, this port is unavailable
    else:
        raise pytest.skip(f"Unable to find an available port on {host}")

    return port


@pytest.fixture(scope="function", name="port")
def next_port(host: str):
    yield next_free_port(host)


@pytest.fixture(scope="function")
def temp_dir() -> tempfile.TemporaryDirectory:
    with tempfile.TemporaryDirectory(
        prefix="heist_asyncssh_", suffix="_test_data"
    ) as temp_dir:
        yield temp_dir

    try:
        os.rmdir(temp_dir)
    except FileNotFoundError:
        ...


@pytest.fixture()
def target_name() -> str:
    return f"heist_test_{uuid.uuid4()}"


@pytest.fixture(scope="function")
async def openssh_container(
    roster: dict,
    network_name: str,
    host: str,
    port: int,
    target_name: str,
    temp_dir: tempfile.TemporaryDirectory,
):
    client = docker.from_env()
    sftp_root = "/upload"
    # The test should pass with either method and this is easier than parametrizing the fixture
    username = "user"
    # username = "root"
    password = "pass"
    pugid = "0" if username == "root" else "1000"

    container = client.containers.run(
        # https://hub.docker.com/r/linuxserver/openssh-server
        "linuxserver/openssh-server:latest",
        command=["/bin/sh", "-c", "while true; do sleep 1; done"],
        detach=True,
        ports=None if network_name == "host" else {"2222/tcp": port},
        hostname=host,
        network=network_name,
        environment={
            "PUID": pugid,
            "PGID": pugid,
            "TZ": "Etc/UTC",
            "SUDO_ACCESS": "true",
            "PASSWORD_ACCESS": "true",
            "USER_NAME": username,
            "USER_PASSWORD": password,
        },
        volumes={temp_dir: {"bind": sftp_root, "mode": "rw"}},
    )
    # Enable SSH Tunneling
    command = (
        "sed -i 's/^AllowTcpForwarding no/AllowTcpForwarding yes/' /etc/ssh/sshd_config"
    )
    container.exec_run(cmd=command, privileged=True, detach=True)
    if username == "root":
        command = "sed -i 's/#PermitRootLogin prohibit-password/PermitRootLogin yes/' /etc/ssh/sshd_config"
        container.exec_run(cmd=command, privileged=True, detach=True)

    try:
        container.reload()
        roster[target_name] = {
            "host": host,
            "id": host,
            "manage_service": False,
            "port": port,
            "username": username,
            "password": password,
            "known_hosts": None,
            "bootstrap": True,
            "sftp_root": sftp_root,
            "sudo": False if username == "root" else True,
        }
        for _ in range(60):
            try:
                async with asyncssh.connect(
                    host,
                    port=port,
                    username=username,
                    password=password,
                    known_hosts=None,
                ):
                    break
            except:
                time.sleep(1)
        else:
            raise pytest.skip("Could not connect to container")

        yield roster[target_name]
    except KeyboardInterrupt:
        ...
    finally:
        container.stop()
        container.remove()
