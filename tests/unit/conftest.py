from unittest import mock

import pytest


def mock_valid_service_names():
    return ("test-service",)


@pytest.fixture(scope="function")
def hub(hub):
    hub.pop.sub.add(dyne_name="heist")
    hub.service.init.valid_service_names = mock_valid_service_names
    yield hub


@pytest.fixture(scope="function")
def mock_hub(hub):
    m_hub = hub.pop.testing.mock_hub()
    m_hub.OPT = mock.MagicMock()
    m_hub.SUBPARSER = "salt.minion"
    m_hub.SUBCOMMANDS = "salt.minion"
    yield m_hub
