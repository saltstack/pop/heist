import sys
from pathlib import PurePosixPath
from unittest.mock import call

import pytest


@pytest.mark.asyncio
async def test_start_service(hub, mock_hub):
    """
    Test starting a systemd service
    """
    target_name = "1234"
    service_name = "test-service"
    mock_hub.service.systemd.start = hub.service.systemd.start
    await mock_hub.service.systemd.start(target_name, "asyncssh", service_name)
    assert mock_hub.tunnel.asyncssh.cmd.call_args == call(
        target_name, f"systemctl start {service_name}"
    )


@pytest.mark.asyncio
async def test_start_service_block_is_false(hub, mock_hub):
    """
    Test starting a systemd service when block is False
    """
    target_name = "1234"
    service_name = "test-service"
    mock_hub.service.systemd.start = hub.service.systemd.start
    await mock_hub.service.systemd.start(
        target_name, "asyncssh", service_name, block=False
    )
    assert mock_hub.tunnel.asyncssh.cmd.call_args == call(
        target_name, f"systemctl start {service_name} --no-block"
    )


@pytest.mark.asyncio
async def test_disable_service(hub, mock_hub):
    """
    Test disabling a systemd service
    """
    target_name = "1234"
    service_name = "test-service"
    mock_hub.service.systemd.disable = hub.service.systemd.disable
    await mock_hub.service.systemd.disable("asyncssh", target_name, service_name)
    assert mock_hub.tunnel.asyncssh.cmd.call_args == call(
        target_name, f"systemctl disable {service_name}"
    )


@pytest.mark.asyncio
async def test_enable_service(hub, mock_hub):
    """
    Test enabling a systemd service
    """
    target_name = "1234"
    service_name = "test-service"
    mock_hub.service.systemd.enable = hub.service.systemd.enable
    await mock_hub.service.systemd.enable("asyncssh", target_name, service_name)
    assert mock_hub.tunnel.asyncssh.cmd.call_args == call(
        target_name, f"systemctl enable {service_name}"
    )


@pytest.mark.asyncio
async def test_enable_service(hub, mock_hub):
    """
    Test stopping a systemd service
    """
    target_name = "1234"
    service_name = "test-service"
    mock_hub.service.systemd.enable = hub.service.systemd.enable
    await mock_hub.service.systemd.enable("asyncssh", target_name, service_name)
    assert mock_hub.tunnel.asyncssh.cmd.call_args == call(
        target_name, f"systemctl enable {service_name}"
    )


@pytest.mark.asyncio
async def test_stop_service(hub, mock_hub):
    """
    test stopping a systemd service
    """
    target_name = "1234"
    service_name = "test-service"
    mock_hub.service.systemd.stop = hub.service.systemd.stop
    await mock_hub.service.systemd.stop(target_name, "asyncssh", service_name)
    assert mock_hub.tunnel.asyncssh.cmd.call_args == call(
        target_name, f"systemctl stop {service_name}"
    )


@pytest.mark.asyncio
async def test_restart_service(hub, mock_hub):
    """
    test restarting a systemd service
    """
    target_name = "1234"
    service_name = "test-service"
    mock_hub.service.systemd.restart = hub.service.systemd.restart
    await mock_hub.service.systemd.restart(target_name, "asyncssh", service_name)
    assert mock_hub.tunnel.asyncssh.cmd.call_args == call(
        target_name, f"systemctl restart {service_name}"
    )


@pytest.mark.skipif(
    sys.platform == "win32",
    reason="path_convert function not supported by Windows yet.",
)
def test_conf_path(hub, mock_hub):
    """
    test conf_path when target is linux
    """
    service_name = "test-service"
    mock_hub.service.systemd.conf_path = hub.service.systemd.conf_path
    mock_hub.tool.path.path_convert = hub.tool.path.path_convert
    ret = mock_hub.service.systemd.conf_path(service_name)
    assert ret == PurePosixPath("/etc", "systemd", "system", "test-service.service")


@pytest.mark.asyncio
async def test_clean(hub, mock_hub):
    """
    test cleaning a systemd service
    """
    target_name = "1234"
    service_name = "minion"
    mock_hub.service.systemd.clean = hub.service.systemd.clean
    await mock_hub.service.systemd.clean(target_name, "asyncssh", service_name)
    assert mock_hub.tunnel.asyncssh.cmd.call_args == call(
        target_name, "systemctl daemon-reload"
    )


@pytest.mark.asyncio
async def test_status_service(hub, mock_hub):
    """
    test getting status of a systemd service
    """
    target_name = "1234"
    service_name = "test-service"
    mock_hub.service.systemd.status = hub.service.systemd.status
    await mock_hub.service.systemd.status(
        target_name=target_name, tunnel_plugin="asyncssh", service=service_name
    )
    assert mock_hub.tunnel.asyncssh.cmd.call_args == call(
        target_name, f"systemctl status {service_name}"
    )
